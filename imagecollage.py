# Collage the images
 
from PIL import Image
import numpy as np

img1 = Image.open("images/dog1.jpg")
img2 = Image.open("images/dog2.jpeg")

img1_array = np.array(img1)
img2_array = np.array(img2)

imgg = np.hstack([img1_array,img2_array])

finalimg = Image.fromarray(imgg)

finalimg.save("images/dogcollage.jpg")

print("Image saved")
